-- Read more about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/user_input/buttons.html


module Main exposing (..)

import Html exposing (..)
import Html.Events exposing (..)


main =
    beginnerProgram { model = initModel, view = view, update = update }



-- MODEL


type alias Model =
    { country1 : Country
    , country2 : Country
    }


type alias Country =
    { name : String
    , soldiers : Int
    }


initModel : Model
initModel =
    { country1 = Country "Australia" 20000
    , country2 = Country "Japan" 50000
    }



-- UPDATE


type Msg
    = Tick
    | Attack String String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Tick ->
            model

        Attack countryAttackingName countryToAttackName ->
            let
                country1 =
                    model.country1

                country2 =
                    model.country2

                country1_ =
                    if country1.name == countryToAttackName then
                        { country1 | soldiers = 0 }
                    else if country1.name == countryAttackingName then
                        { country1 | soldiers = 1000 }
                    else
                        country1

                country2_ =
                    if model.country2.name == countryToAttackName then
                        { country2 | soldiers = 0 }
                    else if model.country2.name == countryAttackingName then
                        { country2 | soldiers = 1000 }
                    else
                        country2
            in
            { model
                | country1 = country1_
                , country2 = country2_
            }



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick Tick ] [ text "Tick" ]
        , viewCountry model.country1 model.country2.name
        , viewCountry model.country2 model.country1.name
        ]


viewCountry : Country -> String -> Html Msg
viewCountry country countryToAttack =
    div []
        [ h2 [] [ text country.name ]
        , div
            []
            [ strong [] [ text "Soldiers: " ]
            , text (toString country.soldiers)
            ]
        , button
            [ onClick (Attack country.name countryToAttack) ]
            [ text ("Attack " ++ countryToAttack ++ "!!!") ]
        ]
